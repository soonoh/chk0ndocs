# Git Guide

## Branch

브랜치는 작업공간이다.
팀은 적절한 브랜치 사용 전략을 확립하고 공유해야 한다.
DevOps 등은 그 전략에 따라 코드를 배포 해야 한다.

### Branch 전략

#### git flow 전략

[git flow 전략](https://nvie.com/posts/a-successful-git-branching-model/)

* 여러 관리 브랜치를 둔다.
* 개발과 배포를 분리한다.
* 여러 버전이 동시에 운영된다.


#### github flow 전략

[github flow 전략](https://guides.github.com/introduction/flow/)

* 하나의 관리 브랜치를 둔다.
* 작업 브랜치는 리뷰되고 스테이지 환경에 배포하고 테스트한다.
* 관리 브랜치에 머지하고 배포한다.

#### TBD 전략

[Trunk Based Development](https://trunkbaseddevelopment.com/)

* github flow 전략과 유사
* CI, [feature toggle](https://martinfowler.com/articles/feature-toggles.html) 등이 강조됨

  

## Rebase Vs Merge

둘다 서로 다른 작업 결과를 어떻게 합칠 것인가에 대한 전략이다.
그 전략의 핵심은 작업 내역의 보존 여부 이다.

* Rebase - 새로 작성
* Merge - 보존

### Rebase

브랜치 병합시 병합할 브랜치의 마지막 커밋으로 현재의 커밋들을 모두 재적용하여 새로운 커밋들을 만들어 내 모든 커밋이 선형적인 이력이 되도록 한다. 

* 작업을 하나의 완전한 변경셋으로 만들때 사용
* 코듀 리뷰등으로 지저분해진 작업을 정리하여 하나의 완결된 작업으로 만들때 사용
* upstream branch pull 할 경우, `pull --rebase` 사용
* 개인 작업 branch 를 공용 branch 로 머지할때 사용.

```
o---A---B---C---D              main-branch
             \
              +---E---F        working-branch

git checkout working-branch
git rebase main-branch


o---A---B---C---D              main-branch
                 \
                  +---E'---F'  working-branch

git checkout main-branch
git merge working-branch

o---A---B---C---D---E'---F'    main-branch
```

### Merge

브랜치 병합시 merge commit 을 만든다.
merge commit 은 일반 커밋과 다르게 이전 커밋이 두개(일반적으로 두 브랜치를 함치므로)가 된다. 따라서 `git revert`시 어느 이전 커밋으로 revert 할지 선택해야 한다.

* 작업들의 그룹이 필요할때 사용
* 작은 여러 완결된 작업을 하나의 큰 기능으로 묶어서 배포할때 사용
* 변경 히스토리를 있는 그대로 유지하고 싶을때 사용

```
o---A---B---C---D              main-branch
             \
              +---E---F        working-branch

git checkout main-branch
git merge working-branch

o---A---B---C---D-----------M  main-branch
             \             /
              +---E---F---+
```

## 기본 사용법

최소한의 사용법을 명령어 위주로 설명한다.

### init, clone, remote

git 은 분산 리포지토리이지만, 대부분은 central 한 remote repo를 두고 대부분 개발자가 자신의 PC 에 가져와서 작업한다.

```plantuml
skinparam backgroundColor transparent

actor developer1
actor developer2
node remoteRepo {
  file master as originMaster
}
node repo as d1Node {
  file master as d1Master
}
node repo as d2Node {
  file master as d2Master
}

d1Master --> originMaster :push
d1Master <-- originMaster :pull
remoteRepo --> d2Node     :clone
originMaster --> d2Master   :pull
originMaster <-- d2Master   :push
developer1 --> d1Master
developer2 --> d2Master
```

`git init` 은 현재 디렉토리를 git 관리하에 두는 명령이다.
최초로 프로젝트를 만드는 사람이 소스 관리를 시작할때 하고, init 후에 나오는 물음에 대해 대부분은 기본값을 사용하면 된다.

`git clone {git repo url}`

`git remote add {remote name} {git repo url}`

### 작업

```plantuml
skinparam backgroundColor transparent

actor developer
node localRepo {
  file master
  file newFeature {
    file staged
    file unstaged
    file committed
  }
}

developer --> localRepo
master --> newFeature    :git checkout -b new-feature
unstaged --> staged      :git add .
staged --> committed     :git commit -m "{commit message}"
newFeature --> master    :git checkout master
```

새로운 브랜치 생성 및 생성된 브랜치로 이동

`git checkout -b {new branch name}`

작업후 작업 내용 저장, 이때는 명령줄 보다 UI 툴을 사용하는 것이 좋을 것이다.

* `git status` - 현재 작업 공간의 상태 확인, 변경사항, staged 된 상황, untracked 상황
* `git add .` - 모든 변경 사항 추가
* `git commit -m "{커밋 메세지}"` - 커밋 메세지 작성

작업후 리뷰 전/후

* `git checkout {merge target branch name}` - 브랜치 이동
* `git pull {remote merge target branch name} {local merge target branch name}` - local 브랜치 최신화
* `git checkout {working branch name}` - 브랜치 이동
* `git rebase {merge target branch name}` - 현재 브랜치 변경 사항을 target branch 마지막 브랜치 부터 적용한다.
* `git push (--force) {remote repo name} {remote working branch name}` - remote 브랜치에 적용, rebase 했다면, --force 로 업데이트해야 한다. 따라서 작업 브랜치는 항상 자신만 작업한다고 생각해야 한다. 다른 사람에게 공유하면 안된다.

작업 완료

* `git rebase -i {merge target branch name}` - 메인 브랜치에 머지하기 전에는 현재 브랜치의 하나로 합치고 적절한 커밋 메세지로 바꾸는 것이 좋다.

### git stash

* `git stash -u` - untracked 파일 포함 모든 작업 내용(not committed)을 stash stack 에 넣고, 현재 작업 공간은 마지막 커밋 상태로 만듬
* `git stash list` - stash stack 에 있는 작업 리스트 보기
* `git stash pop` - stash stack 에서 하나의 작업 내용을 꺼내 현재 브랜치에 적용한다.

### 전체 작업 flow

작업 전 working-branch 생성
```
git checkout -b working-branch && git push origin working-branch

o---A---B---C                  main-branch
             \
              +                working-branch
```
작업
```
git commit -m "New feature: xxx" && git push origin working-branch

o---A---B---C                  main-branch
             \
              +---D            working-branch
```
다른 사람 코드 머지
```
git merge main-branch && git push origin working-branch

o---A---B---C-------E          main-branch
             \       \
              +---D---M        working-branch
```
추가 작업후 코드 리뷰
```
git commit -m "Change xxx" && git push origin working-branch

o---A---B---C-------E          main-branch
             \       \
              +---D---M---F    working-branch
```
코드 리뷰 추가 수정
```
git commit -m "Fix review issue xxx"

o---A---B---C-------E              main-branch
             \       \
              +---D---M---F---G    working-branch
```
코드 리뷰 완료, 코드 정리
```
git rebase main-branch

o---A---B---C-------E                     main-branch
                     \
                      +---D'---F'---G'    working-branch

git rebase -i main-branch && git push --force origin working-branch

o---A---B---C-------E                     main-branch
                     \
                      +---DFG             working-branch

git checkout main-branch
git merge working-branch
git branch -d working-branch # delete local working-branch
git push origin :working-branch # delete remote working-branch

o---A---B---C-------E---DFG               main-branch
```


## UI TOOL

* [git-cola](https://git-cola.github.io/) - 간단한 python 프로그램으로 commit 전 변경 사항을 확인하고 stage, unstage 를 라인단위로 할 수 있다.
* [gitg](https://wiki.gnome.org/Apps/Gitg/) - 기본 적인 git 의 기능을 제공하는 GUI client

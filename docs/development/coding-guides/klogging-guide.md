# Kotlin Logger 사용법

## 사용

[Kotlin Logging](https://github.com/MicroUtils/kotlin-logging)

gradle:

`implementation "io.github.microutils:kotlin-logging:$kotlinLoggingVersion"`

```kotlin
class Test {
    companion object: KLogging() // (1)
    
    fun test() {
        logger.info("Hello, Logger!") // (2)
    }
}
```

* (1) logger 변수를 사용할 수 있게 됨
* (2) KLogging 에서 생성된 logger 를 사용

## Examples

메세지 결합이 필요 없을 경우

`logger.info("message")`

메세지 결합이 필요한 경우

`logger.info { "message $a" }`

메세지 결합과 stack trace 가 필요한 경우

`logger.warn(throwable) { "message $a" }`

## 설명

`logger.info("message a={}", a)` 로 사용시 a 의 값에 `{}` 문자열이 있으면 로깅이 반복되는 버그가 있는 것으로 보임, 또한, `{}` 치환자마다 정확한 수의 인수를 뒤에 넣어 줄때 실수의 가능성이 있다.
따라서 메세지 결합시에는 lazy 방식으로 사용하는 것이 더 Kotlin 방식에 맞아 보임.

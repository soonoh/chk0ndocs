# SpringBoot Properties

## 문제

SpringBoot 의 application.yml 을 설정할때, 문제는 사용자가 설정해야할 부분이 spring 과 그외 라이브러리드에 분리 되어 어떤 부분을 실제로 설정해야 하는지 알기 쉽지 않다는 것이다.

아래의 예를 보자 앱에서 설정해야 되는 것은 아래 세군데에 펼쳐져 있다.

* (1) spring.cloud.gateway.routes
* (2) akka
* (3) feign

위 세군의 설정을 `app` 설정 루트 아래에 두면 더 보기 쉽고 편할 것이다. 

만약 이런 설정이 많이 필요 없다면 (0) 부분 처럼 SpringBoot 의 Placeholder 를 통해 참조해도 될것이다.

```yaml
app:
  name: my-app
  property1: p1
  sub:
    sub-property1: sp1
spring:
  application:
    name: ${app.name} # // (0)
  main:
    allow-bean-definition-overriding: true
  cloud:
    gateway:
      routes: # // (1)
        - id: info
          uri: http://localhost:${server.port}
          predicates:
            - Method=GET
            - Path=/info
          filters:
            - RewritePath=/info,/actuator/info
management:
  endpoints:
    web:
      base-path: /actuator
      exposure.include: 'info,health,mappings'
  endpoint:
    health:
      show-details: always
akka: # // (2)
  stream:
    alpakka:
      spring:
        web:
          actor-system-name: ${spring.application.name}-akka

feign: # // (3)
  client:
    config:
      default:
        connectTimeout: 2000
        readTimeout: 2000
logging:
  level:
    root: INFO
```

## YAML Anchor 와 Alias

그러나 tree 전체를 참조 하고 싶다면, Yaml 의 Anchor와 Alias 를 사용하여 아래와 같이 작성할 수 있다.
이제 여러 루트 property 에 펼쳐진 설정이 app 설정 아래에 통합되었다.

```yaml
app:
  name: my-app
  property1: p1
  sub:
    sub-property1: sp1
  routes: &app-routes # // (1)
    - id: info
    uri: http://localhost:${server.port}
    predicates:
      - Method=GET
      - Path=/info
    filters:
      - RewritePath=/info,/actuator/info
  akka: &app-akka # // (2)
    stream:
      alpakka:
        spring:
          web:
            actor-system-name: ${spring.application.name}-akka
  feign: &app-feign # // (3)
    client:
      config:
        default:
          connectTimeout: 2000
          readTimeout: 2000
spring:
  application:
    name: ${app.name} # // (0)
  main:
    allow-bean-definition-overriding: true
  cloud:
    gateway:
      routes: *app-routes
management:
  endpoints:
    web:
      base-path: /actuator
      exposure.include: 'info,health,mappings'
  endpoint:
    health:
      show-details: always
akka: *app-akka
feign: *app-feign
logging:
  level:
    root: INFO
```

## Merge Key

만약 반복 되는 프로퍼티들을 설정할때, 일부 프로퍼티만 바꾸고 싶다면, Merge Key ("<<") 와 Alias 를 같이 사용할 수 있다.

```yaml
app:
  caches:
    default: &default
      enabled: true
      idle-timeout: 30s
      max-item: 100000
    cache-1: &small
      <<: *default
      max-item: 50000
    cache-2:
      <<: *default
      idle-timeout: 10s
```
